## PRTG Device Template for monitoring Palo Alto Firewalls via SNMP with PRTG

This project is a custom device template that can be used to monitor Palo Alto Firewalls in PRTG using the auto-discovery for simplified sensor creation.

## Download
A zip file containing the template and necessary additional files can be downloaded from the repository using the link below:
- [Latest Version of the Template](https://gitlab.com/PRTG/Device-Templates/PaloAlto/-/jobs/artifacts/master/download?job=PRTGDistZip)

## Installation Instructions
Please refer to INSTALL.md or refer to the following KB-Post:
- [How can I monitor Palo Alto firewalls with PRTG?](https://kb.paessler.com/en/topic/79293)